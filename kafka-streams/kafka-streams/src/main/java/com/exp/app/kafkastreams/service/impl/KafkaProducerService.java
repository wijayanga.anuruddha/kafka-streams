package com.exp.app.kafkastreams.service.impl;

import com.exp.app.kafkastreams.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducerService {

    private static final Logger logger = LoggerFactory.getLogger(KafkaProducerService.class);

    @Value("${app.kafka-topic}")
    private String kafkaTopic;

    private final KafkaTemplate<String, User> kafkaTemplate;

    @Autowired
    public KafkaProducerService(KafkaTemplate<String, User> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendUser(User user) {
        kafkaTemplate.send(kafkaTopic, user);
        logger.info("$$ -> Adding user to kafka -->{}", user);
    }
}
