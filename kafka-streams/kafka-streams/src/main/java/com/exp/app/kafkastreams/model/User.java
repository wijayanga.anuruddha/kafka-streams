package com.exp.app.kafkastreams.model;

import lombok.Data;

@Data
public class User {

    private String fName;
    private String lName;
    private String email;
    private String userName;
    private String password;
    private String contactName;
    private String address;

    public User() {
    }

    public User(String fName, String lName, String email, String userName, String password, String contactName, String address) {
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.userName = userName;
        this.password = password;
        this.contactName = contactName;
        this.address = address;
    }
}
