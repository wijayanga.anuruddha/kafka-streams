package com.exp.app.kafkastreams.service.impl;

import com.exp.app.kafkastreams.model.User;
import org.springframework.kafka.annotation.KafkaListener;

public class KafkaConsumerService {

    @KafkaListener(topics = "${app.kafka-topic}")
    public void consumeMessage(User user) {
        //..do something..
    }
}
