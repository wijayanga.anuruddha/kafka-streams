package com.exp.app.kafkastreams.service.impl;

import com.exp.app.kafkastreams.model.User;
import com.exp.app.kafkastreams.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private KafkaProducerService kafkaProducer;

    @Override
    public void userDataMapping(User user) {
        // what ever the logic implement and send the object to kafka topic
        logger.info("$$ -> user send to kafka --> {}", user);
        kafkaProducer.sendUser(user);
    }
}
