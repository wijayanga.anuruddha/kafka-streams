package com.exp.app.kafkastreams.service;

import com.exp.app.kafkastreams.model.User;

public interface UserService {

    void userDataMapping(User user);
}
